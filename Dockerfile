FROM alpine/git as clone
WORKDIR /app
RUN git clone https://gitlab.com/raphael.moita/spring-docker.git

FROM maven:3.5-jdk-8-alpine as build
WORKDIR /app
COPY --from=clone /app/spring-docker /app
RUN mvn clean package

FROM openjdk:8-jre-alpine
WORKDIR /app
COPY --from=build /app/target/spring-docker*.jar /app/app.jar
ENTRYPOINT ["java", "-jar", "/app/app.jar"]